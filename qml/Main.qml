import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.8

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'postbank.vtb'
    automaticOrientation: false

    anchors.fill: parent
    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Почта Банк Онлайн')
            SequentialAnimation on y {
            	PropertyAnimation { running: webView.loading === false; to: -header.height; duration: 2500 }
            }
        }

        WebEngineView {
			id: webView
			anchors.fill: parent
			url: "https://my.pochtabank.ru/login"
			zoomFactor: 3.0
	
			settings.accelerated2dCanvasEnabled: true
			settings.autoLoadIconsForPage: false
			settings.allowWindowActivationFromJavaScript: true
			settings.showScrollBars: false
	
			profile: WebEngineProfile {
			    httpUserAgent: "Mozilla/5.0 (Linux; Android 11; SM-A125U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Mobile Safari/537.36"
			    storageName: "Storage"
			    persistentStoragePath: "/home/phablet/.local/share/postbank.vtb/QWebEngine"
			    persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
			}
	
			onNewViewRequested : function (request) {
			    request.action = WebEngineNavigationRequest.IgnoreRequest
			    Qt.openUrlExternally(request.requestedUrl)
			}
	
			onJavaScriptDialogRequested: (request) => {
			    request.accepted = true;
			    jsDialog.message = request.message
			    jsDialog.accepted.connect(() => request.dialogAccept())
			    jsDialog.rejected.connect(() => request.dialogReject())
			    jsDialog.open()
			}
	
			onLoadingChanged: {
			    if (loading === false) {
			        //window.showFullScreen(true)
					header.visible = false
			    }
			}
		}
    }

    Item {
        id: webViewLoadingIndicator
        anchors.fill: parent
        z: 1
        visible: webView.loading === true

        BusyIndicator {
            id: busy
            anchors.centerIn: parent
        }
    }

}
