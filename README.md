# Pochta Bank WEB (Почта Банк WEB)

Everyday payments, transfers, loans, savings tools and more without installing app

Повседневные платежи, переводы, кредиты, инструменты для накопления и многое другое без установки нативного приложения

## License

Copyright (C) 2023  Герман Семенов

Licensed under the BSD license.
